/*
 Name:		PCA9622DR_Example.ino
 Created:	9/24/2018 7:41:23 AM
 Author:	andy
*/


#include <wire.h>
#include "PCA9622DR.h"


#define PCA9622DR_MIN_CHANNEL         0
#define PCA9622DR_MAX_CHANNEL         15
#define PCA9622DR_CHANNEL_COUNT       16

// Pin um den PWM Chip zu sperren
#define OEPin 3 // this is Port D3


PCA9622DR PLED01;

// the setup function runs once when you press reset or power the board
void setup() {

	Serial.begin(9600);

	// Lock the PWM Chip for Init
	pinMode(OEPin, OUTPUT);
	digitalWrite(OEPin, HIGH);

	// Wire must be started!
	Wire.begin();

	PLED01.resetDevices();
	PLED01.init(PLED01_I2C_Address, PCA9622DR_MODE1_AI2 | PCA9622DR_MODE1_ALLCALL, PCA9622DR_LDR2, PCA9622DR_LDR2, PCA9622DR_LDR2, PCA9622DR_LDR2);

	// Disable all Led's
	for (uint8_t i_ = 0; i_ < PCA9622DR_MAX_CHANNEL; i_++)
		PLED01.SetChannelOff(i_);

	// Enable the PWM Chip
	digitalWrite(OEPin, LOW);
}

uint8_t PWMValue = 0;
int16_t direction = 1;

// the loop function runs over and over again until power down or reset
void loop() {

	PLED01.SetChannelPWM(1, PWMValue);
	PLED01.SetChannelPWM(2, PWMValue);
	delay(100);
	PWMValue += direction;

	Serial.println(PWMValue);
	if (PWMValue == 255) {
		direction = -1;
		delay(2000);
	}
	
	if (PWMValue == 0) {
		direction = 1;
		delay(2000);
	}
}



