/*  Arduino Library for the PCA9622DR 16-Channel PWM Driver Module.
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

PCA9622DR-Arduino - Version 1.0.0
*/

#ifndef _PCA9622DR_h
#define _PCA9622DR_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#include <wire.h>

#define PCA9622DR_MIN_CHANNEL         0
#define PCA9622DR_MAX_CHANNEL         15
#define PCA9622DR_CHANNEL_COUNT       16

// Pin um den PWM Chip zu sperren
#define OEPin 3 // this is Port D3

// #define PCA9622DR_ENABLE_DEBUG_OUTPUT

// I2C Base Address
#define PLED01_I2C_Address					(byte)0x70

#define PCA9622DR_I2C_RESET_ADDR			(byte)0x06	//  Software Reset Adresse der PLED01 (PCA9622) LED-Controller
#define PCA9622DR_I2C_ALLCALL_ADDR			(byte)0xE0	// "All Call"-Adresse des PCA9622. �ber diese Adresse nimmt den Befehl jedes am I2C-Bus h�ngende LED-Controller-Modul des gleichen Typs entgegen.


#define PCA9622DR_AUTOINC_NO				(byte)0x00			// no Auto-Increment
#define PCA9622DR_AUTOINC_ALL				(byte)0x80			// Auto-Increment all

// Register addresses from data sheet
#define PCA9622DR_MODE1_REG                 (byte)0x00
#define PCA9622DR_MODE2_REG                 (byte)0x01

#define PCA9622DR_SUBADR1_REG               (byte)0x18
#define PCA9622DR_SUBADR2_REG               (byte)0x19
#define PCA9622DR_SUBADR3_REG               (byte)0x1A

#define PCA9622DR_PPWM0_REG					(byte)0x02
#define PCA9622DR_PPWM1_REG					(byte)0x03
#define PCA9622DR_PPWM2_REG  				(byte)0x04
#define PCA9622DR_PPWM3_REG  				(byte)0x05
#define PCA9622DR_PPWM4_REG  				(byte)0x06
#define PCA9622DR_PPWM5_REG 				(byte)0x07
#define PCA9622DR_PPWM6_REG  				(byte)0x08
#define PCA9622DR_PPWM7_REG  				(byte)0x09
#define PCA9622DR_PPWM8_REG 				(byte)0x0A
#define PCA9622DR_PPWM9_REG  				(byte)0x0B
#define PCA9622DR_PPWM10_REG 				(byte)0x0C
#define PCA9622DR_PPWM11_REG 				(byte)0x0D
#define PCA9622DR_PPWM12_REG  				(byte)0x0E
#define PCA9622DR_PPWM13_REG  				(byte)0x0F
#define PCA9622DR_PPWM14_REG  				(byte)0x10
#define PCA9622DR_PPWM15_REG  				(byte)0x11

#define PCA9622DR_LEDOUT0_REG               (byte)0x14			// LED driver output state register (LED 0 - 3)
#define PCA9622DR_LEDOUT1_REG               (byte)0x15			// LED driver output state register (LED 4 - 7)
#define PCA9622DR_LEDOUT2_REG               (byte)0x16			// LED driver output state register (LED 8 - 11)
#define PCA9622DR_LEDOUT3_REG               (byte)0x17			// LED driver output state register (LED 12 - 15)

#define PCA9622DR_GRPPWM_REG                (byte)0x12
#define PCA9622DR_GRPFREQ_REG               (byte)0x13

#define PCA9622DR_SW_RESET					(byte)0x05			// Sent to address PCA9622DR_I2C_RESET_ADDR to reset all devices on Wire line

// Mode1 register
#define PCA9622DR_MODE1_AI2					(byte)0x80
#define PCA9622DR_MODE1_AI1					(byte)0x40
#define PCA9622DR_MODE1_AI0					(byte)0x20
#define PCA9622DR_MODE1_SLEEP				(byte)0x10
#define PCA9622DR_MODE1_SUB1				(byte)0x08
#define PCA9622DR_MODE1_SUB2				(byte)0x04
#define PCA9622DR_MODE1_SUB3				(byte)0x02
#define PCA9622DR_MODE1_ALLCALL				(byte)0x01

// Mode2 register
#define PCA9622DR_MODE2_DMBLNK				(byte)0x20			// group control = blinking
#define PCA9622DR_MODE2_OCH					(byte)0x08			// outputs change on ACK
#define PCA9622DR_MODE2_Reserved			(byte)0x05			// If you change these bits from their default values, the device will not perform as expected.

// Default Settings for the Mode1 
#define PCA622DR_MODE1_Default				PCA9622DR_MODE1_AI2 | PCA9622DR_MODE1_SLEEP | PCA9622DR_MODE1_ALLCALL
#define PCA622DR_MODE2_Default				PCA9622DR_MODE2_Reserved

// LED driver output states
#define PCA9622DR_LDR0						(byte)0x00			// All LEDs off
#define PCA9622DR_LDR1						(byte)0x55			// All LEDs on at 100%
#define PCA9622DR_LDR2						(byte)0xaa			// LEDs an mit individuellen Helligkeit des jeweiligen LED-Kanals
#define PCA9622DR_LDR3						(byte)0xff			// LEDs an mit individuellen Helligkeit des jeweiligen LED-Kanals samt Gesamthelligkeit oder Blinken


class PCA9622DR {
public:
	PCA9622DR();
	void init(byte I2CAddress, byte LEDOUT0, byte LEDOUT1, byte LEDOUT2, byte LEDOUT3);
	void init(byte I2CAddress, byte Mode1, byte LEDOUT0, byte LEDOUT1, byte LEDOUT2, byte LEDOUT3);

#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
	void checkForErrors();
#endif
	void resetDevices();

	uint8_t  GetI2CAddress();
	uint8_t getLastI2CError();

	void SetChannelOn(uint8_t channel);
	void SetChannelOff(uint8_t channel);
	void SetChannelPWM(uint8_t channel, uint8_t pwmAmount);

private:
	uint8_t _i2cAddress;           // Module's i2c address
	uint8_t _lastI2CError;         // Last i2c error

	void writeRegister(byte regAddress, byte value);
	uint8_t readRegister(byte regAddress);

};


#endif
