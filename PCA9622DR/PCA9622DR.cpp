/*  Arduino Library for the PCA9622DR 16-Channel PWM Driver Module.
Copyright (c) 2018 andy      <don_funk@bluewin.ch>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Created by Andreas Zogg, September 26, 2018.

PCA9622DR-Arduino - Version 1.0.0
*/
#include "PCA9622DR.h"


PCA9622DR::PCA9622DR() {
	_i2cAddress = 0;
	_lastI2CError = 0;
}

void PCA9622DR::init(byte I2CAddress, byte LEDOUT0, byte LEDOUT1, byte LEDOUT2, byte LEDOUT3) {

	_i2cAddress = I2CAddress;

	// LED output setting
	writeRegister(PCA9622DR_LEDOUT0_REG, LEDOUT0);
	writeRegister(PCA9622DR_LEDOUT1_REG, LEDOUT1);
	writeRegister(PCA9622DR_LEDOUT2_REG, LEDOUT2);
	writeRegister(PCA9622DR_LEDOUT3_REG, LEDOUT3);
}

void PCA9622DR::init(byte I2CAddress, byte Mode1, byte LEDOUT0, byte LEDOUT1, byte LEDOUT2, byte LEDOUT3) {

	_i2cAddress = I2CAddress;

	// Zuerst wird der allgemeine Betriebsmodus konfiguriert
	writeRegister(PCA9622DR_MODE1_REG, Mode1);
	delayMicroseconds(50);

	// LED output setting
	writeRegister(PCA9622DR_LEDOUT0_REG, LEDOUT0);
	writeRegister(PCA9622DR_LEDOUT1_REG, LEDOUT1);
	writeRegister(PCA9622DR_LEDOUT2_REG, LEDOUT2);
	writeRegister(PCA9622DR_LEDOUT3_REG, LEDOUT3);
}

#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT

static const char *textForI2CError(byte errorCode) {
	switch (errorCode) {
	case 0:
		return "Success";
	case 1:
		return "Data too long to fit in transmit buffer";
	case 2:
		return "Received NACK on transmit of address";
	case 3:
		return "Received NACK on transmit of data";
	default:
		return "Other error";
	}
}

void PCA9622DR::checkForErrors() {
	if (_lastI2CError) {
		Serial.print("  PCA9685::checkErrors lastI2CError: ");
		Serial.print(_lastI2CError);
		Serial.print(": ");
		Serial.println(textForI2CError(getLastI2CError()));
	}
}

#endif

void PCA9622DR::resetDevices() {
#ifdef PC9622DR_ENABLE_DEBUG_OUTPUT
	Serial.println("PCA9622DR::resetDevices");
#endif
	Wire.beginTransmission(PCA9622DR_I2C_RESET_ADDR);
	Wire.write(PCA9622DR_SW_RESET);
	_lastI2CError = Wire.endTransmission();

	delayMicroseconds(10);
}

uint8_t PCA9622DR::GetI2CAddress() {
	return _i2cAddress;
}

uint8_t PCA9622DR::getLastI2CError() {
	return _lastI2CError;
}

void PCA9622DR::SetChannelOn(uint8_t channel) {
	if (channel < 1 || channel > 16) return;

#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
	Serial.println("PCA9622DR::SetLEDOn");
#endif

	writeRegister(PCA9622DR_PPWM0_REG + channel - 1, 255);
}

void PCA9622DR::SetChannelOff(uint8_t channel) {
	if (channel < 1 || channel > 16) return;

#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
	Serial.println("PCA9622DR::SetLEDOff");
#endif

	writeRegister(PCA9622DR_PPWM0_REG + channel - 1, 0);
}

void PCA9622DR::SetChannelPWM(uint8_t channel, uint8_t pwmAmount) {
	if (channel < 1 || channel > 16) return;

#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
	Serial.println("PCA9622DR::SetLEDPWM");
#endif

	writeRegister(PCA9622DR_PPWM0_REG + channel - 1, pwmAmount);
}

void PCA9622DR::writeRegister(byte regAddress, byte value) {
#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
	Serial.print("  PCA9622DR::writeRegister regAddress: 0x");
	Serial.print(regAddress, HEX);
	Serial.print(", value: 0x");
	Serial.print(value, HEX);
	Serial.print(", i2cAddress: 0x");
	Serial.println(_i2cAddress, HEX);
#endif

	Wire.beginTransmission(_i2cAddress);
	Wire.write(regAddress);
	Wire.write(value);
	_lastI2CError = Wire.endTransmission();

#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
	checkForErrors();
#endif
}

byte PCA9622DR::readRegister(byte regAddress) {
#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
	Serial.print("  PCA9622DR::readRegister regAddress: 0x");
	Serial.println(regAddress, HEX);
#endif

	Wire.beginTransmission(_i2cAddress);
	Wire.write(regAddress);
	_lastI2CError = Wire.endTransmission();
	if (_lastI2CError != 0) {
#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
		checkForErrors();
#endif
		return 0;
	}

	int bytesRead = Wire.requestFrom((uint8_t)_i2cAddress, (uint8_t)1);
	if (bytesRead != 1) {
		while (bytesRead-- > 0)
			Wire.read();
		_lastI2CError = 4;
#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
		checkForErrors();
#endif
		return 0;
	}

	byte retVal = Wire.read();

#ifdef PCA9622DR_ENABLE_DEBUG_OUTPUT
	Serial.print("    PCA9622DR::readRegister retVal: 0x");
	Serial.println(retVal, HEX);
#endif

	return retVal;
}

